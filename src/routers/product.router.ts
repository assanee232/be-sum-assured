import { getProduct } from "../controllers/product.controller";
import { Router } from "express";
const router = Router();

router.post("/", getProduct);

export default router;
