import express from "express";
import personRouter from "./routers/product.router";
import { json } from "body-parser";
const cors = require("cors");

const server = express();
const PORT = 3333;

server.use(cors());

server.use(json());

server.use("/product", personRouter);

server.listen(PORT, () => console.log(`Server is started at port ${PORT}`));
