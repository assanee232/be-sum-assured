import { RequestHandler } from "express";

const getPaymentFrequency = (paymentFrequency: string) => {
  let paymentFrequencyRate = 0;
  switch (paymentFrequency) {
    case "YEARLY":
      paymentFrequencyRate = 26.03;
      break;
    case "HALFYEARLY":
      paymentFrequencyRate = 13.015;
      break;
    case "QUARTERLY":
      paymentFrequencyRate = 6.5075;
      break;
    case "MONTHLY":
      paymentFrequencyRate = 2.16916667;
      break;
  }
  return paymentFrequencyRate;
};

export const getProduct: RequestHandler = (req, res, next) => {
  const fixedRate: number = 1000;
  const paymentFrequency = getPaymentFrequency(req.body.paymentFrequency);
  let baseSumAssured: number = 0;

  if (req.body.calculateType === "assured") {
    baseSumAssured = (req.body.saPerYear * paymentFrequency) / fixedRate;
  } else {
    baseSumAssured = (req.body.premiumPerYear * fixedRate) / paymentFrequency;
  }

  res.status(200).json({ ...req.body, baseSumAssured });
};
