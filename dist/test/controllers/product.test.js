"use strict";
const _sum = require("../../controllers/product.controller");
describe("test sum function", () => {
    test("adds 1 + 2 to equal 3", () => {
        expect(_sum.add(1, 2)).toBe(3);
    });
});
