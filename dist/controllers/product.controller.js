"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getProduct = void 0;
const getPaymentFrequency = (paymentFrequency) => {
    let paymentFrequencyRate = 0;
    switch (paymentFrequency) {
        case "YEARLY":
            paymentFrequencyRate = 26.03;
            break;
        case "HALFYEARLY":
            paymentFrequencyRate = 13.015;
            break;
        case "QUARTERLY":
            paymentFrequencyRate = 6.5075;
            break;
        case "MONTHLY":
            paymentFrequencyRate = 2.16916667;
            break;
    }
    return paymentFrequencyRate;
};
const getProduct = (req, res, next) => {
    const fixedRate = 1000;
    const paymentFrequency = getPaymentFrequency(req.body.paymentFrequency);
    let baseSumAssured = 0;
    if (req.body.calculateType === "assured") {
        baseSumAssured = (req.body.saPerYear * paymentFrequency) / fixedRate;
    }
    else {
        baseSumAssured = (req.body.premiumPerYear * fixedRate) / paymentFrequency;
    }
    res.status(200).json(Object.assign(Object.assign({}, req.body), { baseSumAssured }));
};
exports.getProduct = getProduct;
