"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPersons = void 0;
// import Person from "../models/Person";
// สมมติให้ PERSONS เป็นตัวแทนของข้อมูลที่ได้มาจาก Database
// โดยไม่มีการคำนึงถึงเรื่องระยะเวลาที่ต้องรอให้ข้อมูลมาถึง Person controller
// const PERSONS = [
//   new Person("1613735097256", "John", "Wick", 32),
//   new Person("1613735129810", "Thomas", "Anderson", 30),
//   new Person("1613736126066", "John", "Constantine", 35),
// ];
// เปลี่ยนจากการประกาศ getPersons แบบ Declaration Function เป็นแบบ Expression Function
const getPersons = (req, res, next) => {
    res.json({ persons: { aa: "aaฟa" } });
};
exports.getPersons = getPersons;
// export const createPerson: RequestHandler = (req, res, next) => {
//   const { firstName, lastName, age } = <Person>req.body;
//   const newPerson = new Person(String(Date.now()), firstName, lastName, age);
//   try {
//     PERSONS.push(newPerson);
//     res
//       .status(201)
//       .json({ message: `Created person success`, createdPerson: newPerson });
//   } catch (err) {
//     res.status(500).json({ errorMessage: "Something went wrong" });
//   }
// };
// export const updatePerson: RequestHandler<{ id: string }> = (
//   req,
//   res,
//   next
// ) => {
//   const id = req.params.id;
//   const foundPerson: Person | undefined = PERSONS.find((p) => p.id === id);
//   if (!foundPerson) {
//     res.status(404).send({ errorMessage: "Person not found" });
//   }
//   const personFromReq = <Person>req.body;
//   const updatedPerson = Object.assign(foundPerson, personFromReq);
//   res.json({
//     message: `Updated person ID ${id} success`,
//     updatedPerson: updatedPerson,
//   });
// };
// export const deletePerson: RequestHandler<{ id: string }> = (
//   req,
//   res,
//   next
// ) => {
//   const id = req.params.id;
//   const personIdx = PERSONS.findIndex((person) => id === person.id);
//   // ถ้า PERSON.findIndex() แล้วเจอข้อมูลใน array จะ return index กลับมา แต่ถ้าไม่เจอจะ return -1
//   if (personIdx < 0) {
//     res.status(404).json({ errorMessage: "Person not found" });
//   }
//   PERSONS.splice(personIdx, 1);
//   res.json({ message: `Deleted person ID ${id} success` });
// };
