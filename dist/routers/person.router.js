"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const person_controller_1 = require("../controllers/person.controller");
const express_1 = require("express");
const router = (0, express_1.Router)();
router.get("/", person_controller_1.getPersons);
// router.post("/", createPerson);
// router.patch("/:id", updatePerson);
// router.delete("/:id", deletePerson);
exports.default = router;
