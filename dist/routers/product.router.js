"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const product_controller_1 = require("../controllers/product.controller");
const express_1 = require("express");
const router = (0, express_1.Router)();
router.post("/", product_controller_1.getProduct);
exports.default = router;
