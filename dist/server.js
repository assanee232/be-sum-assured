"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const product_router_1 = __importDefault(require("./routers/product.router"));
const body_parser_1 = require("body-parser");
const cors = require("cors");
const server = (0, express_1.default)();
const PORT = 3333;
server.use(cors());
server.use((0, body_parser_1.json)());
server.use("/product", product_router_1.default);
server.listen(PORT, () => console.log(`Server is started at port ${PORT}`));
